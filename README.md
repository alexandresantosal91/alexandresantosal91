<p align="center"><img src="src/wallpaper.jpg">

## Olá, sou Alexandre Santos 👋

Sou um estudante de Tecnologia em Análise e Desenvolvimento de Sistemas com um foco especializado em cibersegurança na posição júnior.

### Sobre Mim:

Minha paixão pela tecnologia floresceu desde o momento em que tive meu primeiro contato com computadores aos 12 anos. Desde então, minha jornada tem sido impulsionada pela busca incessante de conhecimento nessa área.

O que me motiva é o desejo de adquirir uma compreensão profunda da cibersegurança para poder contribuir na proteção das pessoas e empresas contra as crescentes ameaças cibernéticas. Atualmente, estou imerso no estudo de ferramentas essenciais, como o Kali Linux e o Wireshark, esta última sendo uma poderosa ferramenta de código aberto amplamente reconhecida para análise de redes e muito mais.

Estou constantemente em busca de novas oportunidades para aprender e crescer na minha área de atuação. Se você tiver alguma sugestão ou feedback para compartilhar, por favor, sinta-se à vontade para entrar em contato comigo.

### Entre em Contato:

Você pode me encontrar pelos seguintes meios:

* **E-mail:** [alexandresantos_al@icloud.com](mailto:alexandresantos_al@icloud.com)
* **LinkedIn:** [Meu perfil no LinkedIn](https://www.linkedin.com/in/alexandresantosal/)
* **Bio.link:** [Meu Bio.link](https://linktr.ee/alexandresantosal)

### Análise do GitHub ⚙️

| ![Estatísticas](http://github-profile-summary-cards.vercel.app/api/cards/stats?username=alexandresantosal91&theme=nord_dark) | ![Repositórios por Linguagem](http://github-profile-summary-cards.vercel.app/api/cards/repos-per-language?username=alexandresantosal91&hide=Html&theme=nord_dark) | ![Linguagem mais Usada](http://github-profile-summary-cards.vercel.app/api/cards/most-commit-language?username=alexandresantosal91&theme=nord_dark) |
| :-: | :-: | :-: |

| ![Detalhes do Perfil](http://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=alexandresantosal91&theme=nord_dark) | ![Estatísticas de Atividade](https://github-readme-streak-stats.herokuapp.com/?user=alexandresantosal91&hide_border=true&date_format=M%20j%5B%2C%20Y%5D&background=2D3742&stroke=2D3742&ring=6bbbca&fire=6bbbca&currStreakNum=fff&sideNums=6bbbca&currStreakLabel=6bbbca&sideLabels=fff&dates=fff) |
| :-: | :-: |

</p>

<p align="center"><img src="https://profile-counter.glitch.me/alexandresantosal91/count.svg">
